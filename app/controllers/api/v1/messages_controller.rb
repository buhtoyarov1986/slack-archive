class Api::V1::MessagesController < ApplicationController
  def create
    message_creator.call
    render json: { challenge: params[:challenge] }, status: :created
  end

  def search
    messages = Message.where('message LIKE(?)', "%#{params[:text]}%").pluck(:message).join(', ')
    render json: { result:  messages }, status: :ok
  end

  private

  def message_creator
    MessageCreator.new(message_params)
  end

  def message_params
    return {} if params[:event].blank?
    params.require(:event)
  end
end
