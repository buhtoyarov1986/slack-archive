Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resource :messages do
        collection { post :search }
      end
    end
  end
end
