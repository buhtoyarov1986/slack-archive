class MessageCreator
  attr_reader :user, :obj

  def initialize(obj)
    @obj = JSON.parse(obj.to_json, object_class: OpenStruct)
    find_or_create_user
  end

  def call
    create_or_update_message
  end

  private

  def find_or_create_user
    @user = User.find_or_create_by(slack_id: user_id)
  end

  def create_or_update_message
    message = user.messages.find_or_initialize_by(ext_id: message_id).tap do |message|
      message.message = text
    end

    message.save
  end

  def message_changed?
    obj.subtype == 'message_changed'
  end

  def text
    message_changed? ? obj.message.text : obj.text
  end

  def message_id
    message_changed? ? obj.message.client_msg_id : obj.client_msg_id
  end

  def user_id
    message_changed? ? obj.message.user : obj.user
  end
end