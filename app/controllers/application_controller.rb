class ApplicationController < ActionController::API
  before_action :check_token

  private

  def check_token
    if params['token'] != ENV['SLACK_API_TOKEN']
      render json: { errors: 'token invalid' }, status: :unauthorized
    end
  end
end
