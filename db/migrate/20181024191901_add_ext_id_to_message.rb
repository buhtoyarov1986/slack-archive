class AddExtIdToMessage < ActiveRecord::Migration[5.2]
  def change
    add_column :messages, :ext_id, :string
    add_index :messages, :ext_id
  end
end
